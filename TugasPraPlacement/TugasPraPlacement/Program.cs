﻿//TugasNo1();
//TugasNo2();
//TugasNo3();
//TugasNo4();
//TugasNo5();
//TugasNo6();
//TugasNo7();
//TugasNo8();
//TugasNo9();
//TugasNo10();
//TugasNo11();
//TugasNo12();
//TugasNo13();
//TugasNo14();
//TugasNo15();
//TugasNo16();
//TugasNo17();
//TugasNo18();
//TugasNo19();
//TugasNo20();
//TugasNo21();
//TugasNo22();

Console.ReadKey();

static void TugasNo1()
{
    int[] kacamata = { 500, 600, 700, 800 };
    int[] baju = { 200, 400, 350 };
    int[] sepatu = { 400, 350, 200, 300 };
    int[] buku = { 100, 50, 150 };

    int budget = 1000;
    int bill = 0;
    int selPrev = 0;
    int selisihbudget = 0;


    List<string> belanja = new List<string>();

    for (int i = 0; i < kacamata.Length; i++)
    {
        for (int j = 0; j < baju.Length; j++)
        {
            for (int k = 0; k < sepatu.Length; k++)
            {
                for (int l = 0; l < buku.Length; l++)
                {
                    bill = kacamata[i] + baju[j] + sepatu[k] + buku[l];
                    selisihbudget = budget - bill;
                    if (bill <= 1000 && selisihbudget <= selPrev)
                    {
                        belanja.Clear();
                        belanja.Add($"kacamata {kacamata[i]}");
                        belanja.Add($"baju {baju[j]}");
                        belanja.Add($"sepatu {sepatu[k]}");
                        belanja.Add($"buku {buku[l]}");

                        selPrev = selisihbudget;
                    }


                }
            }
        }
    }

    Console.WriteLine($"\nJumlah item : {belanja.Count()} ({String.Join(", ", belanja)})");

}
static void TugasNo2()
{
    int a = 14; 
    int b = 3;
    int c = 7;
    int d = 7;

    int denda = 100;
    int totalDenda = 0;

    int cek;

    Console.WriteLine("Parsing Datetime");
    Console.Write("Masukkan Peminjaman (dd/MM/yyyy) : ");
    string datestring = Console.ReadLine();
    Console.Write("Masukkan Pengembalian (dd/MM/yyyy) : ");
    string datestring1 = Console.ReadLine();

    DateTime date1 = DateTime.ParseExact(datestring, "d/M/yyyy", null);
    DateTime date2 = DateTime.ParseExact(datestring1, "d/M/yyyy", null);

    TimeSpan interval = date2-date1;
    int Days = interval.Days;

    cek = Days - a;
    if (cek > 0)
    {
        totalDenda += cek * denda;
    }
    cek = Days - b;
    if (cek > 0)
    {
        totalDenda += cek * denda;
    }
    cek = Days - c;
    if (cek > 0)
    {
        totalDenda += cek * denda;
    }
    cek = Days - d;
    if (cek > 0)
    {
        totalDenda += cek * denda;
    }

    Console.WriteLine("Jumlah Denda : " + totalDenda);
}

static void TugasNo3()
{
    double totaltarif = 0;
    Console.WriteLine("Parsing Datetime");
    Console.Write("Waktu Masuk (dd/MM/yyyy|HH/mm/ss): ");
    string datestring = Console.ReadLine();
    Console.Write("Waktu Keluar (dd/MM/yyyy): ");
    string datestring1 = Console.ReadLine();

    DateTime date1 = DateTime.ParseExact(datestring, "d/M/yyyy|HH/mm/ss", null);
    DateTime date2 = DateTime.ParseExact(datestring1, "d/M/yyyy|H/m/s", null);

    TimeSpan interval = date2 - date1;

    double inDays = interval.TotalSeconds;
    double jam = inDays / 3600;
    double hari = jam / 24;

    if(hari <= 1)
    {
        if(jam <= 8)
        {
            totaltarif += Math.Ceiling(jam) * 1000;
        }
        else
        {
            totaltarif = 8000;
        }
    }
    else
    {
        double sisajam = Math.Ceiling(jam - 24) * 1000;
        totaltarif = 15000 + sisajam;
    }

    Console.WriteLine("Jumlah Tarif : " + totaltarif);
}
static void TugasNo4()
{
    int ulang, sisa, jumlahbilangan;
    int cek = 0;
    int tes = 1;
    List<int> list = new List<int>();
    Console.Write("Masukkan n = ");
    int bil = int.Parse(Console.ReadLine());
    for (int i = 0; i < bil;)
    {
        jumlahbilangan = 0;
        for (ulang = 1; ulang <= tes; ulang++)
        {
            sisa = tes % ulang;
            if (sisa == 0)
            {
                jumlahbilangan = jumlahbilangan + 1;
            }
            else
            {
                jumlahbilangan = jumlahbilangan;
            }
        }
        if (jumlahbilangan < 3)
        {
            list.Add(tes);
            i++;
        }
        tes++;
    }
    Console.WriteLine(bil + "bilangan prima pertama");
    foreach (int item in list)
    {
        Console.WriteLine(item + "");
    }
}
static void TugasNo5()
{
    Console.Write("Masukkan N : ");
    int n = int.Parse(Console.ReadLine());
    int[] fibonaci = new int[n];
    int a = 0, b = 1, x = 1;
    Console.WriteLine(n + " Bilangan Fibonacci Pertama");
    for (int i = 0; i < n; i++)
    {
       
        Console.Write(x + " ");
        x = a + b;
        a = b;
        b = x;
    }
}

static void TugasNo6()
{
    Console.Write("Masukkan Kata : ");
    string k = Console.ReadLine();
    int panjang = k.Length;
    bool kata = true;
    for (int i = 0; i < panjang/2;i++)
    {
        if (k[i] != k[panjang-i-1])
        {
            kata = false; 
            break;
        }
       
    }
    if (kata)
    {
        Console.WriteLine($"{k} adalah palindrome");
    }
    else 
    {
        Console.WriteLine($"{k} bukan palindrome");
    }
}

static void TugasNo7()
{
    Console.Write("Masukkan angka : ");
    string input = Console.ReadLine();
    // Membaca dan mengonversi input string menjadi array angka
    int[] deretAngka = input.Trim().Split(' ').Select(int.Parse).ToArray();
    Array.Sort(deretAngka);
    // Menghitung Mean
    float mean = 0;
    float hitung = 0;
    for (int i = 0; i < deretAngka.Length; i++) 
    { 
        hitung += deretAngka[i];
    }
    mean = hitung / deretAngka.Length;
    // Menghitung Median
    float median = 0;

    if (deretAngka.Length % 2 == 0)
    {
        median = deretAngka[(deretAngka.Length / 2) - 1] + deretAngka[(deretAngka.Length / 2)];
        median = median / 2;
    }
    else
    {
        median = deretAngka[(deretAngka.Length / 2)];
    }
    //menghitung Modus
    int modus = 0;
    int t = 0;
    for (int a = 0; a < deretAngka.Length; a++)
    {
        int m = 0;
        for (int j = a; j < deretAngka.Length; j++)
        {
            if (deretAngka[j] == deretAngka[a])
            {
                m++;
            }
        }
        if (m > t)
        {
            t = m ;
            modus = deretAngka[a];
        }
    }


    Console.WriteLine($"mean = {mean}");
    Console.WriteLine($"median = {median}");
    Console.WriteLine($"modus = {modus}");

}

static void TugasNo8()
{
    List<int> list = new List<int>() { 1, 2, 4, 7, 8, 6, 9 };

    List<int> maxList = list.GetRange(0, list.Count);
    int totalmax = 0;

    List<int> minList = list.GetRange(0, list.Count);
    int totalmin = 0;

    for (int i = 0; i < 4; i++)
    {
        int max = 0;
        for (int j=0; j<maxList.Count; j++)
        {
            if (maxList[j]>max)
            {
                max = maxList[j];
            }
        }
        totalmax += max;
        maxList.Remove(max);

        int min = int.MaxValue;
        for (int j=0;j<minList.Count; j++)
        {
            if (minList[j]<min)
            {
                min = minList[j];
            }
        }
        totalmin += min;
        minList.Remove(min);
    }
    Console.WriteLine(totalmax);
    Console.WriteLine(totalmin);
}

static void TugasNo9()
{
    Console.WriteLine("Deret Angka");
    Console.Write("Masukkan N : ");
    int n = int.Parse( Console.ReadLine());
    
    for (int i = 0; i < n; i++)
    {
        if (i== 0)
        {
            Console.Write($"{n} ");
        }
        else
        {
            Console.Write($"{n + n*i} ");
        }
    }
}

static void TugasNo10()
{
    Console.WriteLine("Program Mengganti Kata dengan *");
    Console.Write("Masukkan Kalimat : ");
    string kalimat = Console.ReadLine();
    string[] katakata = kalimat.Split(" ");
    for (int i = 0; i < katakata.Length; i++)
    {
        for (int j = 0; j < katakata[i].Length; j++)
        {
            if (j == 0 || j == katakata[i].Length - 1)
            {
                Console.Write(katakata[i][j]);
            }
            else
            {
                Console.Write("*");
            }
        }
        Console.Write(" ");
    }
}

static void TugasNo11()
{
    Console.Write("Masukkan Kata : ");
    string input = Console.ReadLine().ToLower();
    for (int i = input.Length - 1; i >= 0; i--)
    {
        if (input.Length % 2 == 0)
        { 
            string bintang = new string('*', 3);
            string output = bintang + input[i] + bintang;
            Console.WriteLine($"{output}");
        }
        else
        {
            string bintang = new string('*', 2);
            string output = bintang + input[i] + bintang;
            Console.WriteLine($"{output}");
        }
    }
}

static void TugasNo12()
{
    Console.Write("Masukkan angka-angka : ");
    string input = Console.ReadLine();
    int[] angka = input.Trim().Split(' ').Select(int.Parse).ToArray();

    for (int i = 0; i < angka.Length - 1; i++)
    {
        for (int j = 0; j < angka.Length - 1 - i; j++)
        {
            if (angka[j] > angka[j + 1])
            {
                // Menukar angka jika angka sebelumnya lebih besar
                int tamp = angka[j];
                angka[j] = angka[j + 1];
                angka[j + 1] = tamp;
            }
        }
    }
    Console.Write("Hasil pengurutan : ");
    for (int i = 0; i < angka.Length; i++)
    {
        Console.Write(angka[i]);
        if (i < angka.Length - 1)
        {
            Console.Write(" ");
        }
    }
}

static void TugasNo13()
{
    Console.Write("masukan Jam (H:mm) = ");
    string jam = Console.ReadLine();
    int[] array = Array.ConvertAll(jam.Split(':'), int.Parse);

    int x = array[0];
    int y = array[1];

    double hasil = Math.Abs((0.5 * (60 * x + y)) - (6 * y));

    if (hasil > 180)
    {
        hasil = 360 - hasil;
    }
    Console.WriteLine("Jam " + jam + " -> " + hasil);

}

static void TugasNo14()
{
    Console.WriteLine("Program Memindahkan Angka");
    Console.Write("Masukkan angka  : ");
    string[] input = Console.ReadLine().Trim().Split(" ");
    Console.Write("Masukkan rot: ");
    int rot = int.Parse(Console.ReadLine());
    Console.Write($"N = {rot} -> ");

        for (int j = rot; j < input.Length; j++)
        {
            Console.Write(input[j] + " ");
        }
        for (int a = 0; a < rot; a++)
        {
            Console.Write(input[a]);
            if (a != rot - 1)
            {
                Console.Write(" ");
            }
        }
        Console.WriteLine();
}

static void TugasNo15()
{
    Console.WriteLine("Time Conversion");
    Console.Write("Masukkan Waktu (hh:mm:ss AM/PM): ");
    string waktu = Console.ReadLine().ToUpper();
    try
    {
        DateTime date1 = DateTime.ParseExact(waktu, "hh:mm:ss tt", null);
        Console.WriteLine(date1.ToString("HH:mm:ss"));
    }
    catch (Exception e)
    {
        Console.WriteLine("Format yang anda masukkan salah!!");
        Console.WriteLine("Pesan error : " + e.Message);
    }
}

static void TugasNo16()
{
    List<string> list = new List<string>();
    List<char> ikanlist = new List<char>();
    List<double> dou = new List<double>();

cek:
    Console.Write("masukan Jenis Makanan = ");
    string kata = Console.ReadLine();
    list.Add(kata);

    Console.Write("Mengandung Ikan ? y/t = ");
    char ik = Convert.ToChar(Console.ReadLine());
    ikanlist.Add(ik);

    Console.Write("masukan harga = ");
    double harga = Convert.ToDouble(Console.ReadLine());
    dou.Add(harga);

    Console.Write("Tambah Makanan ? ya/tidak = ");
    string jawab = Console.ReadLine().ToLower();

    if (jawab == "ya")
    {
        goto cek;
    }

    double a = 0;
    double b = 0;

    for (int i = 0; i < list.Count; i++)
    {
        if (ikanlist[i] == 'y')
        {
            a += dou[i];
        }
        else
        {
            b += dou[i];
        }
    }

    double q = a * 10 / 100;
    double w = a * 5 / 100;
    double e = b * 10 / 100;
    double r = b * 5 / 100;

    double p = (a + b) * 10 / 100;
    double s = (a + b) * 5 / 100;
    double total = a + b;
    double bayar = a + b + p + s;

    double ptpt = (b + e + r) / 4;
    double pt = ptpt + ((a + q + w) / 3);

    for (int i = 0; i < list.Count; i++)
    {
        if (ikanlist[i] == 'y')
        {
            Console.WriteLine($"{list[i]} *ikan {dou[i]}");
        }
        else
        {
            Console.WriteLine($"{list[i]} {dou[i]}");
        }
    }
    Console.WriteLine("Total Harga Pesanan = " + total);
    Console.WriteLine("Pajak 10% = " + p);
    Console.WriteLine("Servis 5% = " + s);
    Console.WriteLine("Total Pembayaran = " + bayar);
    Console.WriteLine("Patungan 3 Orang yang tidak alergi = " + pt);
    Console.WriteLine("Patungan 1 Orang yang alergi = " + ptpt);

}

static void TugasNo17()
{
    Console.Write("Masukkan N/T = ");
    string[] x = Console.ReadLine().Split(' ');

    int gunung = 0;
    int lembah = 0;

    for (int i = 0; i < x.Length - 1; i++)
    {
        if (x[i] == "N" && x[i + 1] == "T")
        {
            gunung++;
        }
        if (x[i] == "T" && x[i + 1] == "N")
        {
            lembah++;
        }
    }

    Console.WriteLine($"Gunung {gunung} dan Lembah {lembah}");
}

static void TugasNo18()
{
    List<double> kalori = new List<double>();
    List<double> jam = new List<double>();

cek:

    Console.Write("Masukan Jam = ");
    double ik = Convert.ToDouble(Console.ReadLine());
    jam.Add(ik);

    Console.Write("Jumlah Kalori = ");
    double kal = Convert.ToDouble(Console.ReadLine());
    kalori.Add(kal);

    Console.Write("Tambah kue ? ya/tidak = ");
    string jawab = Console.ReadLine().ToLower();



    if (jawab == "ya")
    {
        goto cek;
    }
    Console.Write("Olahraga Jam = ");
    double ol = Convert.ToDouble(Console.ReadLine());

    Console.WriteLine("    Jam       Kalori");

    for (int i = 0; i < jam.Count; i++)
    {
        if (jam[i] < 10)
        {
            Console.WriteLine("      " + jam[i] + "        " + kalori[i]);
        }
        else
        {
            Console.WriteLine("     " + jam[i] + "        " + kalori[i]);
        }

    }

    double waktu = 0;
    double j, w;

    for (int i = 0; i < jam.Count; i++)
    {
        j = ol - jam[i];
        w = 0.1 * kalori[i] * (j * 60);
        waktu += w;
    }
    int nah = Convert.ToInt32(waktu);
    int minum = ((nah / 30) * 100) + 500;

    Console.WriteLine("Perlu Minum Sebanyak " + minum + " cc");
}

static void TugasNo19()
{
    Console.WriteLine("Program Pangram");
    Console.WriteLine("Masukkan Kalimat : ");
    string kalimat = Console.ReadLine().ToLower();
    string alphabet = "abcdefghijklmnopqrstuvwxyz";
    int count = 0;
    for (int i = 0; i < alphabet.Length; i++)
    {
        bool x = kalimat.Contains(alphabet[i]);
        if (x == true)
        {
            count++;
        }
    }
    if (count == 26)
    {
        Console.WriteLine("Kalimat ini adalah pangram");
    }
    else
    {
        Console.WriteLine("Kalimat ini bukan pangram");
    }
}

static void TugasNo20()
{
    List<string> a = new List<string>();
    List<string> b = new List<string>();

cek:

    Console.Write("Suit A = ");
    string sa = Console.ReadLine().ToLower();
    a.Add(sa);

    Console.Write("Suit B = ");
    string sb = Console.ReadLine().ToLower();
    b.Add(sb);

    Console.Write("Suit Lagi ? ya/tidak = ");
    string jawab = Console.ReadLine().ToLower();



    if (jawab == "ya")
    {
        goto cek;
    }

    Console.Write("Jarak Awal = ");
    int jarak = int.Parse(Console.ReadLine());

    int pa = 0; int pb = jarak;

    string win = "";

    for (int i = 0; i < a.Count; i++)
    {
        string ceka = a[i].ToLower();
        string cekb = b[i].ToLower();


        if (ceka == "g" && cekb == "b")
        {
            pb = pb + 1;
            win = "B";
        }
        if (ceka == "g" && cekb == "k")
        {
            pa = pa + 1;
            win = "A";
        }

        if (ceka == "b" && cekb == "k")
        {
            pb = pb + 1;
            win = "B";
        }
        if (ceka == "b" && cekb == "g")
        {
            pa = pa + 1;
            win = "A";
        }

        if (ceka == "k" && cekb == "b")
        {
            pa = pa + 1;
            win = "A";
        }
        if (ceka == "k" && cekb == "g")
        {
            pb = pb + 1;
            win = "B";
        }

        if (pa == pb)
        {
            Console.WriteLine("Yang Menang = " + win);
            break;
        }
    }

}


static void TugasNo21()
{
    Console.WriteLine("---DISTANCE---");
    Console.Write("Masukan Perjalanan : ");
    string pola = Console.ReadLine();

    List<char> list = new List<char>();

    int goal = pola.Length - 6;
    int d = 0;
    int s = 0;

    for (int i = 0; i <= goal; i++)
    {
        if (pola[i + 1] == 'O' || pola[i] == 'O' && s >= 2)
        {
            d += 3;
            i += 2;
            s -= 2;
            list.Add('J');
        }
        else if (d + 2 == goal)
        {
            d += 2;
            i += 2;
            s -= 2;
            list.Add('J');
        }
        else if (pola[i] == 'O' && s < 2)
        {
            break;
        }
        else if (pola[i] == '_')
        {
            list.Add('W');
            d++;
            s++;
        }
    }

    if (d == goal)
    {
        for (int i = 0; i < list.Count; i++)
        {
            Console.Write(list[i] + " ");
        }
    }
    else
    {
        Console.WriteLine("FAILED");
    }

}


static void TugasNo22()
{
    Console.WriteLine("nomer 22 tentang lilin yang meleleh");
    Console.Write("masukkan panjang lilin : ");
    string[] panjangLilin = Console.ReadLine().Trim().Split(" ");
    int[] panjangLilinInt = Array.ConvertAll(panjangLilin, int.Parse);
    int[] fibonacci = new int[panjangLilinInt.Length];
    fibonacci[0] = 0;
    fibonacci[1] = 1;

    for (int i = 0; i < fibonacci.Length; i++)
    {
        if (i >= 2)
        {
            fibonacci[i] = fibonacci[i - 1] + fibonacci[i - 2];
        }

    }

    bool end = true;
    do
    {
        for (int i = 0; i < panjangLilinInt.Length; i++)
        {
            panjangLilinInt[i] -= fibonacci[i];
            if (panjangLilinInt[i] <= 0)
            {
                end = false;
                Console.WriteLine($"Lilin yang habis duluan adalah lilin dengan index ke- {i}");
            }
        }
    } while (end);
}
