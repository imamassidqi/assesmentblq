﻿using Apotek.datamodels;
using Apotek.Services;
using Apotek.viewmodels;
using Microsoft.AspNetCore.Mvc;

namespace Apotek.Controllers
{
    public class CategoryController : Controller
    {
        private CategoryService categoryService;
        private int IdUser = 1;

        public CategoryController(CategoryService _categoryService)
        {
            categoryService = _categoryService;
        }
        public async Task<IActionResult> Index(string sortOrder, string searchString, string currentFilter, int? pageNumber, int? pageSize)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrentPageSize = pageSize;
            ViewBag.NameSort = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;

            List<Category> data = await categoryService.GetAllData();

            if (!string.IsNullOrEmpty(searchString))
            {
                data = data.Where(a => a.NameCategory.ToLower().Contains(searchString.ToLower())
                    || a.Description.ToLower().Contains(searchString.ToLower())).ToList();
            }

            switch (sortOrder)
            {
                case "name_desc":
                    data = data.OrderByDescending(a => a.NameCategory).ToList();
                    break;
                default:
                    data = data.OrderBy(a => a.NameCategory).ToList();
                    break;
            }

            return View(PaginatedList<Category>.CreateAsync(data, pageNumber ?? 1, pageSize ?? 3));

        }

        public IActionResult Create()
        {
            Category data = new Category();
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> Create(Category dataParam)
        {
            dataParam.CreatedBy = IdUser;
            VMResponse respon = await categoryService.Create(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return View(dataParam);
        }

        public async Task<JsonResult> CheckNameIsExist(string nameCategory, int id)
        {
            bool isExist = await categoryService.CheckCategoryByName(nameCategory, id);
            return Json(isExist);
        }

        public async Task<IActionResult> Edit(int id)
        {
            Category data = await categoryService.GetDataById(id);
            return PartialView(data);
        }


        [HttpPost]
        public async Task<IActionResult> Edit(Category dataParam)
        {
            dataParam.ModifiedBy = IdUser;
            VMResponse respon = await categoryService.Edit(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }

            return View(dataParam);
        }

        public async Task<IActionResult> Detail(int id)
        {
            Category data = await categoryService.GetDataById(id);
            return PartialView(data);
        }

        public async Task<IActionResult> Delete(int id)
        {
            Category data = await categoryService.GetDataById(id);
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> SureDelete(int id)
        {
            int createBy = IdUser;
            VMResponse respon = await categoryService.Delete(id, createBy);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return RedirectToAction("Index");
        }
    }
}
