﻿using Apotek.datamodels;
using Apotek.viewmodels;
using Newtonsoft.Json;
using System.Text;

namespace Apotek.Services
{
    public class CategoryService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();
        private string json;

        public CategoryService(IConfiguration _configuration)
        {
            configuration = _configuration;
            RouteAPI = configuration["RouteAPI"];
        }

          public async Task<List<Category>> GetAllData()
        {
            List<Category> data = new List<Category>();
            string apiResponse = await client.GetStringAsync(RouteAPI + "apiCategory/GetAllData");
            data = JsonConvert.DeserializeObject<List<Category>>(apiResponse);

            return data;
        }

        public async Task<VMResponse> Create(Category dataParam)
        {
            //proses convert dari object ke string
            string json = JsonConvert.SerializeObject(dataParam);
            //proses mengubah string menjadi json lalu dikirim sebagai request body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            //proses memanggil API dan mengirimkan body
            var request = await client.PostAsync(RouteAPI + "apiCategory/Save", content);
            if (request.IsSuccessStatusCode)
            {
                //proses membaca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();
                //proses convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }

        public async Task<bool> CheckCategoryByName(string nameCategory, int id)
        {
            string apiRespon = await client.GetStringAsync(RouteAPI + $"apiCategory/CheckCategoryByName/{nameCategory}/{id}");
            bool isExist = JsonConvert.DeserializeObject<bool>(apiRespon);
            return isExist;
        }

        public async Task<Category> GetDataById(int id)
        {
            Category data = new Category();
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiCategory/GetDataById/{id}");
            data = JsonConvert.DeserializeObject<Category>(apiResponse);
            return data;
        }

        public async Task<VMResponse> Edit(Category dataParam)
        {
            //proses convert dari object ke string
            string json = JsonConvert.SerializeObject(dataParam);
            //proses mengubah string menjadi json lalu dikirim sebagai request body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            //proses memanggil API dan mengirimkan body
            var request = await client.PutAsync(RouteAPI + "apiCategory/Edit", content);
            if (request.IsSuccessStatusCode)
            {
                //proses membaca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();
                //proses convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }

        public async Task<VMResponse> Delete(int id, int createBy)
        {
            var request = await client.DeleteAsync(RouteAPI + $"apiCategory/Delete/{id}/{createBy}");
            if (request.IsSuccessStatusCode)
            {
                var apiRespon = await request.Content.ReadAsStringAsync();
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else 
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;

        }
    }
    
}
