﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Apotek.viewmodels
{
    public class VMTblProduct
    {
     
        public long Id { get; set; }
    
        public long? IdVariant { get; set; }
   
        public string? NameProduct { get; set; }

        public string NameVariant { get; set; } = null!;
        public long? IdCategory { get; set; }
        public string NameCategory { get; set; } = null!;

        public decimal? Price { get; set; }
       
        public long? Stock { get; set; }
   
        public string? ImagePath { get; set; }
   
        public long CreatedBy { get; set; }
      
        public DateTime CreatedOn { get; set; }
       
        public long? ModifiedBy { get; set; }
       
        public DateTime? ModifiedOn { get; set; }
        
        public long? DeletedBy { get; set; }
      
        public DateTime? DeletedOn { get; set; }
  
        public bool IsDelete { get; set; }

        public IFormFile ImageFile { get; set; }
    }
}
