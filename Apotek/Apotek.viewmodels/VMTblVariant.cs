﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apotek.viewmodels
{
    public class VMTblVariant
    {
        
        public long Id { get; set; }
      
        public long? IdCategory { get; set; }
      
        public string? NameVariant { get; set; }
        public string NameCategory { get; set; } = null!;
        public string? Description { get; set; }
       
        public long CreatedBy { get; set; }
       
        public DateTime CreatedOn { get; set; }
     
        public long? ModifiedBy { get; set; }
       
        public DateTime? ModifiedOn { get; set; }
   
        public long? DeletedBy { get; set; }
      
        public DateTime? DeletedOn { get; set; }
    
        public bool IsDelete { get; set; }
    }

}

