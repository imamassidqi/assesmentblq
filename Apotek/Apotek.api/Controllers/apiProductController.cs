﻿using Apotek.datamodels;
using Apotek.viewmodels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Apotek.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiProductController : ControllerBase
    {
        private readonly DB_ApotekContext db;
        private VMResponse respon = new VMResponse();
        private int IdUser = 1;

        public apiProductController(DB_ApotekContext _db)
        {
            db = _db;
        }
        [HttpGet("GetAllData")]
        public List<VMTblProduct> GetAllData()
        {
            List<VMTblProduct> data = (from p in db.Products
                                       join v in db.Variants on p.IdVariant equals v.Id
                                       join c in db.Categories on v.IdCategory equals c.Id
                                       where p.IsDelete == false
                                       select new VMTblProduct
                                       {
                                           Id = p.Id,
                                           NameProduct = p.NameProduct,
                                           Price = p.Price,
                                           Stock = p.Stock,
                                           ImagePath = p.ImagePath,

                                           IdVariant = p.IdVariant,
                                           NameVariant = v.NameVariant,

                                           IdCategory = v.IdCategory,
                                           NameCategory = c.NameCategory,

                                           CreatedOn = p.CreatedOn
                                       }).ToList();
            return data;
        }

        //Left Join
        [HttpGet("GetAllData_LeftJoin")]
        public List<VMTblProduct> GetAllData_LeftJoin()
        {
            List<VMTblProduct> data = (from p in db.Products
                                       join v in db.Variants on p.IdVariant equals v.Id
                                       join c in db.Categories on v.IdCategory equals c.Id into tc
                                       from tcategory in tc.DefaultIfEmpty()
                                       where p.IsDelete == false && tcategory == null
                                       select new VMTblProduct
                                       {
                                           Id = p.Id,
                                           NameProduct = p.NameProduct,
                                           Price = p.Price,
                                           Stock = p.Stock,
                                           ImagePath = p.ImagePath,

                                           IdVariant = p.IdVariant,
                                           NameVariant = v.NameVariant,

                                           IdCategory = v.IdCategory,
                                           NameCategory = tcategory.NameCategory ?? "",

                                           CreatedOn = p.CreatedOn
                                       }).ToList();
            return data;
        }

        [HttpGet("GetDataById/{id}")]
        public VMTblProduct GetDataById(int id)
        {


            VMTblProduct data = (from p in db.Products
                                 join v in db.Variants on p.IdVariant equals v.Id
                                 join c in db.Categories on v.IdCategory equals c.Id
                                 where p.IsDelete == false && p.Id == id
                                 select new VMTblProduct
                                 {
                                     Id = p.Id,
                                     NameProduct = p.NameProduct,
                                     Price = p.Price,
                                     Stock = p.Stock,
                                     ImagePath = p.ImagePath,

                                     IdVariant = p.IdVariant,
                                     NameVariant = v.NameVariant,

                                     IdCategory = v.IdCategory,
                                     NameCategory = c.NameCategory,

                                     CreatedOn = p.CreatedOn
                                 }).FirstOrDefault()!;
            return data;
        }

        [HttpGet("CheckByName/{name}/{id}/{idVariant}")]
        public bool CheckName(string name, int id, int idVariant)
        {
            Product data = new Product();
            if (id == 0)//ini untuk saat create
            {
                data = db.Products.Where(a => a.NameProduct == name && a.IsDelete == false && a.IdVariant == idVariant).FirstOrDefault();
            }
            else // untuk edit 
            {
                data = db.Products.Where(a => a.NameProduct == name && a.IsDelete == false && a.IdVariant == idVariant && a.Id != id).FirstOrDefault();
            }
            if (data != null)
            {
                return true;
            }
            return false;
        }

        [HttpPost("Save")]

        public VMResponse Save(Product data)
        {
            data.CreatedBy = IdUser;
            data.CreatedOn = DateTime.Now;
            data.IsDelete = false;

            try
            {
                db.Add(data);
                db.SaveChanges();

                respon.Message = "Data success saved";
            }
            catch (Exception ex)
            {
                respon.Success = false;
                respon.Message = "Failed saved" + ex.Message;
            }
            return respon;
        }

        [HttpPut("Edit")]
        public VMResponse Edit(Product data)
        {
            Product dt = db.Products.Where(a => a.Id == data.Id).FirstOrDefault()!;
            if (dt != null)
            {
                dt.NameProduct = data.NameProduct;
                dt.IdVariant = data.IdVariant;
                dt.Price = data.Price;
                dt.Stock = data.Stock;
                if (data.ImagePath != null)
                {
                    dt.ImagePath = data.ImagePath;
                }
                dt.ModifiedBy = IdUser;
                dt.ModifiedOn = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    respon.Message = "Data success saved";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Failed saved : " + ex.Message;
                }
                return respon;
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not found";
            }
            return respon;

        }
        [HttpDelete("Delete/{id}")]

        public VMResponse Delete(int id)
        {
            Product dt = db.Products.Where(a => a.Id == id).FirstOrDefault();
            if (dt != null)
            {

                dt.IsDelete = true;
                dt.ModifiedBy = IdUser;
                dt.ModifiedOn = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    respon.Message = "Data success deleted";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Failed deleted : " + ex.Message;
                }
                return respon;
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not found";
            }
            return respon;

        }
    }
}
