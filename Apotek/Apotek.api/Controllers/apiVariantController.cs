﻿using Apotek.datamodels;
using Apotek.viewmodels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Apotek.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiVariantController : ControllerBase
    {
        private readonly DB_ApotekContext db;
        private VMResponse respon = new VMResponse();
        private int IdUser = 1;
        public apiVariantController(DB_ApotekContext _db)
        {
            db = _db;
        }
        [HttpGet("GetAllData")]
        public List<VMTblVariant> GetAllData()
        {
            List<VMTblVariant> data = (from v in db.Variants
                                       join c in db.Categories on v.IdCategory equals c.Id
                                       where v.IsDelete == false
                                       select new VMTblVariant
                                       {
                                           Id = v.Id,
                                           NameVariant = v.NameVariant,
                                           Description = v.Description,

                                           IdCategory = v.IdCategory,
                                           NameCategory = c.NameCategory,

                                           IsDelete = v.IsDelete,
                                           CreatedOn = v.CreatedOn,
                                       }).ToList();
            return data;
        }

        [HttpGet("GetDataById/{id}")]
        public VMTblVariant GetDataById(int id)
        {
            VMTblVariant data = (from v in db.Variants
                                 join c in db.Categories on v.IdCategory equals c.Id
                                 where v.IsDelete == false && v.Id == id
                                 select new VMTblVariant
                                 {
                                     Id = v.Id,
                                     NameVariant = v.NameVariant,
                                     Description = v.Description,

                                     IdCategory = v.IdCategory,
                                     NameCategory = c.NameCategory,

                                     IsDelete = v.IsDelete
                                 }).FirstOrDefault()!;
            return data;
        }
        [HttpGet("CheckByName/{name}/{id}/{idCategory}")]
        public bool CheckName(string name, int id, int idCategory)
        {
            Variant data = new Variant();
            if (id == 0)
            {
                data = db.Variants.Where(a => a.NameVariant == name && a.IsDelete == false && a.IdCategory == idCategory).FirstOrDefault()!;
            }
            else
            {
                data = db.Variants.Where(a => a.NameVariant == name && a.IsDelete == false && a.IdCategory == idCategory
                && a.Id != id).FirstOrDefault()!;
            }

            if (data != null)//untuk saat edit di front end
            {
                return true;
            }
            return false;
        }

        [HttpGet("GetDataByIdCategory/{id}")]
        public List<VMTblVariant> GetDataByIdCategory(int id)
        {
            List<VMTblVariant> data = (from v in db.Variants
                                       join c in db.Categories on v.IdCategory equals c.Id
                                       where v.IsDelete == false && v.IdCategory == id
                                       select new VMTblVariant
                                       {
                                           Id = v.Id,
                                           NameVariant = v.NameVariant,
                                           Description = v.Description,

                                           IdCategory = v.IdCategory,
                                           NameCategory = c.NameCategory,

                                       }).ToList();
            return data;
        }

        [HttpPost("Save")]
        public VMResponse Save(Variant data)
        {
            data.Description = data.Description ?? "";
            data.CreatedBy = IdUser;
            data.CreatedOn = DateTime.Now;
            data.IsDelete = false;

            try
            {
                db.Add(data);
                db.SaveChanges();

                respon.Message = "Data success saved";
            }
            catch (Exception ex)
            {
                respon.Success = false;
                respon.Message = "Failed Saved : " + ex.Message;
            }

            return respon;
        }

        [HttpPut("Edit")]
        public VMResponse Edit(Variant data)
        {
            Variant dt = db.Variants.Where(a => a.Id == data.Id).FirstOrDefault()!;

            if (dt != null)
            {
                dt.NameVariant = data.NameVariant;
                dt.Description = data.Description;
                dt.IdCategory = data.IdCategory;
                dt.ModifiedBy = IdUser;
                dt.ModifiedOn = DateTime.Now;


                try
                {
                    db.Update(dt);
                    db.SaveChanges();


                    respon.Message = "Data success saved";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Failed saved" + ex.Message;
                }

            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not found";
            }
            return respon;
        }

        [HttpDelete("Delete/{id}")]

        public VMResponse Delete(int id)
        {
            Variant dt = db.Variants.Where(a => a.Id == id).FirstOrDefault()!;

            if (dt != null)
            {
                dt.IsDelete = true;
                dt.ModifiedBy = IdUser;
                dt.ModifiedOn = DateTime.Now;


                try
                {
                    db.Update(dt);
                    db.SaveChanges();


                    respon.Message = "Data success deleted";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Failed deleted" + ex.Message;
                }

            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not found";
            }
            return respon;
        }
        [HttpPut("MultipleDelete")]
        public VMResponse MultipleDelete(List<int> listId)
        {
            if (listId.Count > 0)
            {
                foreach (int item in listId)
                {
                    Variant dt = db.Variants.Where(a => a.Id == item).FirstOrDefault();
                    dt.IsDelete = true;
                    dt.ModifiedBy = IdUser;
                    dt.ModifiedOn = DateTime.Now;
                    db.Update(dt);
                }
                try
                {
                    db.SaveChanges();

                    respon.Message = "Data success deleted";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Failed deleted" + ex.Message;
                }

            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not found";
            }
            return respon;
        }
    }
}
