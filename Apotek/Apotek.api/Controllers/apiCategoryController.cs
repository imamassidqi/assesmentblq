﻿using Apotek.datamodels;
using Apotek.viewmodels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Apotek.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiCategoryController : ControllerBase
    {
        private readonly DB_ApotekContext db;
        private VMResponse respon = new VMResponse();
        private int IdUser = 1;
        public apiCategoryController(DB_ApotekContext _db)
        {
            db = _db;
        }
        [HttpGet("GetAllData")]
        public List<Category> GetAllData()
        {
            List<Category> data = db.Categories.Where(a => a.IsDelete == false).ToList();
            return data;
        }

        [HttpGet("GetDataById/{id}")]
        public Category DataById(int id)
        {
            Category result = db.Categories.Where(a => a.Id == id).FirstOrDefault();
            return result;
        }

        [HttpGet("CheckCategoryByName/{name}/{id}")]
        public bool CheckName(string name, int id)
        {
            Category data = new Category();
            if (id == 0)//ini untuk saat create
            {
                data = db.Categories.Where(a => a.NameCategory == name && a.IsDelete == false).FirstOrDefault();
            }
            else // untuk edit 
            {
                data = db.Categories.Where(a => a.NameCategory == name && a.IsDelete == false && a.Id != id).FirstOrDefault();
            }
            if (data != null)
            {
                return true;
            }
            return false;
        }
        [HttpPost("Save")]

        public VMResponse Save(Category data)
        {
            data.CreatedBy = IdUser;
            data.CreatedOn = DateTime.Now;
            data.IsDelete = false;
            data.Description = data.Description ?? "";

            try
            {
                db.Add(data);
                db.SaveChanges();

                respon.Message = "Data success saved";
            }
            catch (Exception ex)
            {
                respon.Success = false;
                respon.Message = "Failed saved" + ex.Message;
            }
            return respon;
        }

        [HttpPut("Edit")]
        public VMResponse Edit(Category data)
        {
            Category dt = db.Categories.Where(a => a.Id == data.Id).FirstOrDefault();
            if (dt != null)
            {
                dt.NameCategory = data.NameCategory;
                dt.Description = data.Description;
                dt.ModifiedBy = IdUser;
                dt.ModifiedOn = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    respon.Message = "Data success saved";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Failed saved : " + ex.Message;
                }
                return respon;
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not found";
            }
            return respon;

        }

        [HttpDelete("Delete/{id}/{createBy}")]

        public VMResponse Delete(int id, int createBy)
        {
            Category dt = db.Categories.Where(a => a.Id == id).FirstOrDefault();
            if (dt != null)
            {

                dt.IsDelete = true;
                dt.ModifiedBy = createBy;
                dt.ModifiedOn = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    respon.Message = "Data success deleted";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Failed deleted : " + ex.Message;
                }
                return respon;
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not found";
            }
            return respon;

        }

    }
}
